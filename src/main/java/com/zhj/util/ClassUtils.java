package com.zhj.util;


import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ClassUtils {

    private static final String packagePattern = "package\\s+\\S+\\s*;";

    private static final String classNamePattern = "class\\s+\\S+\\s+\\{";

    /**
     * 获取到源码当中定义的完全限定名
     *
     * @param sourceCode
     * @return
     */
    public static String getFullClassName(String sourceCode) {
        String className = "";
        Pattern pattern = Pattern.compile(packagePattern);
        Matcher matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            className = matcher.group().replaceFirst("package", "").replace(";", "").trim() + ".";
        }
        pattern = Pattern.compile(classNamePattern);
        matcher = pattern.matcher(sourceCode);
        if (matcher.find()) {
            className += matcher.group().replaceFirst("class", "").replace("{", "").trim();
        }
        return className;
    }

}
