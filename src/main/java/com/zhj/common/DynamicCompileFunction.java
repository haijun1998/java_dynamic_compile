package com.zhj.common;

import com.zhj.enums.DynamicCompileType;
import com.zhj.wapper.DynamicCompileWrapper;


@FunctionalInterface
public interface DynamicCompileFunction<R extends DynamicCompileWrapper<?>> {

    R get();

    /**
     * 获取当前动态编译的包装类
     * @param dynamicCompileType
     * @return
     */
    default R getOrThrow(DynamicCompileType dynamicCompileType) {
        R r = get();
        if (r == null) {
            throw new UnsupportedOperationException(String.format("不支持当前类型的脚本引擎:%s", dynamicCompileType));
        }
        return r;
    }

}
