package com.zhj;

@FunctionalInterface
public interface DynamicCallBack {

    /**
     * 执行完动态脚本的后置处理逻辑
     * @param value 返回值参数
     */
    void call(Object value);

}
