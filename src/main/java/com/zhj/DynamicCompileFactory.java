package com.zhj;


import com.zhj.common.DynamicCompileFunction;
import com.zhj.enums.DynamicCompileType;
import com.zhj.wapper.DynamicCompileWrapper;
import com.zhj.wapper.DynamicJavaCompileWrapper;
import com.zhj.wapper.DynamicJsCompileWrapper;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class DynamicCompileFactory {

    /**
     * 创建全局引擎池
     */
    private static final Map<DynamicCompileType, DynamicCompileWrapper<?>> enginePool = new ConcurrentHashMap<>();

    public static DynamicCompileWrapper<?> getScriptEngine(DynamicCompileType compileType) {
        return getScriptEngine(() -> {
            DynamicCompileWrapper<?> dynamicCompileWrapper = null;
            if (DynamicCompileType.JS.equals(compileType)) {
                dynamicCompileWrapper = DynamicJsCompileWrapper.createInstance();
            } else if (DynamicCompileType.JAVA.equals(compileType)){
                dynamicCompileWrapper = DynamicJavaCompileWrapper.createInstance();
            }
            return dynamicCompileWrapper;
        }, compileType);
    }

    public static DynamicCompileWrapper<?> getScriptEngine(DynamicCompileFunction<DynamicCompileWrapper<?>> dynamicCompileFunction,
                                                           DynamicCompileType dynamicCompileType) {
        return dynamicCompileFunction.getOrThrow(dynamicCompileType);
    }
}
