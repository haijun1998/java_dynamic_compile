package com.zhj.entity;

import com.zhj.enums.DynamicCompileType;

import lombok.Data;

@Data
public class DynamicCompileEntity {

    private String content;

    private DynamicCompileType dynamicCompileType;

}
