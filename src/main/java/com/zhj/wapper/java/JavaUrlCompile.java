package com.zhj.wapper.java;


import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;

public class JavaUrlCompile {

    private static final String MAIN = "main";

    /**
     * 获取一个java代码编译器
     */
    private static final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();


    /**
     * 编译后生成的内存对象
     */
    private Object targetObj;

    /**
     * 编译耗时
     */
    private long compilerTaskTime;

    /**
     * 设置运行耗时
     */
    private long runTaskTime;

    /**
     * 执行结果
     */
    private String runResult;


}
