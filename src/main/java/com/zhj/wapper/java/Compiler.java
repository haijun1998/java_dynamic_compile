package com.zhj.wapper.java;


import java.util.Map;

/**
 *
 */
public interface Compiler {

    /**
     * Eval
     *
     * @param eval   eval
     * @param params params
     * @return the object
     * @since 1.0.0
     */
    Object eval(String eval, Map<String, Object> params);

    /**
     * Eval for path
     *
     * @param path   path
     * @param params params
     * @return the object
     * @since 1.0.0
     */
    Object evalForPath(String path, Map<String, Object> params);

}
