package com.zhj.wapper;

import com.zhj.exception.DynamicCompileException;

import java.util.Map;
import java.util.concurrent.locks.Lock;


public interface DynamicCompileWrapper<T> {

    /**
     * 执行脚本
     *
     * @param eval 脚本
     * @return
     */
    Object execFunction(String eval, Map<String, Object> params) throws DynamicCompileException;

    /**
     * 指定脚本中指定的方法
     *
     * @param methodName
     * @return
     */
    Object execFunctionForMethodName(String eval, String methodName, Map<String, Object> params) throws DynamicCompileException;

    /**
     * 执行文件中的代码
     * @param path
     * @return
     */
    Object execFunctionForFile(String path, String methodName, Map<String, Object> params) throws DynamicCompileException;

    /**
     * 获取到执行器脚本对象
     *
     * @return
     */
    T getScriptEngine(String scriptName);

    /**
     * 获取到锁信息
     * @return
     */
    Lock getLock();

}
