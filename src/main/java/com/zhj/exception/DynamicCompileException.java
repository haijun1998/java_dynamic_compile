package com.zhj.exception;


public class DynamicCompileException extends Exception {

    private static final long serialVersionUID = -408458257360874029L;

    private static final String MSG = "动态编译失败：%s";

    public DynamicCompileException(String msg) {
        super(msg);
    }

    public static DynamicCompileException newInstance(String msg) {
        return new DynamicCompileException(String.format(MSG, msg));
    }

}
