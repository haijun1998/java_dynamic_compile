package com.zhj;

import com.zhj.enums.DynamicCompileType;
import com.zhj.exception.DynamicCompileException;
import com.zhj.wapper.DynamicCompileWrapper;
import com.zhj.wapper.java.JavaCompile;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import javax.script.ScriptException;

/**
 * Unit test for simple App.
 */
public class DynamicCompileTest
{
    @Test
    public void test_Js_compile() throws ScriptException, NoSuchMethodException, InterruptedException {


        CompletableFuture.runAsync(() -> {
            String str = "function bbb() { print(a); return a}; bbb()";
            Map<String, Object> map = new HashMap<>();
            map.put("a", "hello world");

            DynamicCompileWrapper<?> scriptEngine = DynamicCompileFactory.getScriptEngine(DynamicCompileType.JS);
            try {
                scriptEngine.execFunction(str, map);
            } catch (DynamicCompileException e) {
                e.printStackTrace();
            }
        });

        CompletableFuture.runAsync(() -> {
            String str = "function bbb() { print(a); return a}; ";
            Map<String, Object> map = new HashMap<>();
            map.put("a", "hello world2");
            DynamicCompileWrapper<?> scriptEngine = DynamicCompileFactory.getScriptEngine(DynamicCompileType.JS);
            try {
                scriptEngine.execFunctionForMethodName(str, "bbb", map);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        Thread.currentThread().join();
    }

    @Test
    public void test_java_compile() throws ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        String code = "public class HelloWorld {\n" +
                      "    public void add(String a) {System.out.println(a);}\n" +
                      "    \n" +
                      "}";
        DynamicCompileWrapper<JavaCompile> scriptEngine = (DynamicCompileWrapper<JavaCompile>) DynamicCompileFactory.getScriptEngine(DynamicCompileType.JAVA);
        JavaCompile engine = scriptEngine.getScriptEngine(code);
        engine.runMethod("add", "hello");
    }

    @Test
    public void test_java_exception() throws ClassNotFoundException, InvocationTargetException, IllegalAccessException,
                                           NoSuchMethodException {
        String code = "public class HelloWorld {\n" +
                      "    public void add(String a) {int i = 1 / 0;}\n" +
                      "    \n" +
                      "}";
        DynamicCompileWrapper<JavaCompile> scriptEngine = (DynamicCompileWrapper<JavaCompile>) DynamicCompileFactory.getScriptEngine(DynamicCompileType.JAVA);
        JavaCompile engine = scriptEngine.getScriptEngine(code);
        engine.runMethod("add", "hello");
    }
}
